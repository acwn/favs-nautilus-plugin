/*

Filename: nautilus-favs.c
Copyright  2005 - 2013  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs/

This file is part of Filemanager-AVS.

Filemanager-AVS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Filemanager-AVS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Filemanager-AVS.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nautilus-favs.h"

static GType avs_type = 0;

static void scan_callback (NautilusMenuItem *item, gpointer user_data)
{
	GList *files;
	NautilusFileInfo *file;
	gchar *cmd;
	gchar *uri;
	gchar *path;
		
	/* get path of selected file */
	gnome_vfs_init ();
	files = g_object_get_data (G_OBJECT (item), "files");
	file = files->data;
	uri = nautilus_file_info_get_uri (file);
	path = gnome_vfs_get_local_path_from_uri (uri);
	/* the command and file */
	cmd = g_build_filename (BINDIR, PACKAGE, NULL);
	cmd = g_strconcat (cmd, " --scan=\"", path, "\"", NULL);
	/* make it so */
	g_spawn_command_line_async (cmd, NULL);
	g_free (cmd);
	g_free (uri);
}

static GList *filemanager_avs_get_file_items (NautilusMenuProvider *provider, GtkWidget *widget, GList *files)
{
	GList *items = NULL;
	GList *scan;
	gboolean one_item;
	NautilusMenuItem *item;
	
	for (scan = files; scan; scan = scan->next)
	{
		NautilusFileInfo *file = scan->data;
		gchar *scheme;
		gboolean local;
		
		scheme = nautilus_file_info_get_uri_scheme (file);
		local = strncmp (scheme, "file", 4) == 0;
		g_free (scheme);
		if (!local)
			return NULL;
	}
	one_item = (files != NULL) && (files->next == NULL);
	if (one_item && !nautilus_file_info_is_directory ((NautilusFileInfo*) files->data))
	{
		item = nautilus_menu_item_new ("FileManagerAVS::scan", gettext ("Scan file for viruses"), gettext ("Scan file for viruses"),  avs_get_full_location ("virus-idle.svg"));
		g_signal_connect (item, "activate", G_CALLBACK (scan_callback), provider);
		g_object_set_data_full (G_OBJECT (item), "files", nautilus_file_info_list_copy (files), (GDestroyNotify) nautilus_file_info_list_free);
		items = g_list_append (items, item);
	}
	else
	{
		item = nautilus_menu_item_new ("FilemanagerAVS::scan", gettext ("Scan directory for viruses"), gettext ("Scan directory for viruses"), avs_get_full_location ("virus-idle.svg"));
		g_signal_connect (item, "activate", G_CALLBACK (scan_callback), provider);
		g_object_set_data_full (G_OBJECT (item), "files", nautilus_file_info_list_copy (files), (GDestroyNotify) nautilus_file_info_list_free);
		items = g_list_append (items, item);
	}
	return items;

}

static void filemanager_avs_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
	iface->get_file_items = filemanager_avs_get_file_items;
}

static void filemanager_avs_instance_init (FilemanagerAVS *cvs)
{
	/* nothing to do */
}

static void filemanager_avs_class_init (FilemanagerAVSClass *class)
{
	/* nothing to do */
}

GType filemanager_avs_get_type (void)
{
	return avs_type;
}

void filemanager_avs_register_type (GTypeModule *module)
{
	static const GTypeInfo info =
	{
		sizeof (FilemanagerAVSClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) filemanager_avs_class_init,
		NULL,
		NULL,
		sizeof (FilemanagerAVS),
		0,
		(GInstanceInitFunc) filemanager_avs_instance_init
	};

	static const GInterfaceInfo menu_provider_iface_info =
	{
		(GInterfaceInitFunc) filemanager_avs_menu_provider_iface_init,
		NULL,
		NULL
	};
	avs_type = g_type_module_register_type (module, G_TYPE_OBJECT, "FilemanagerAVS", &info, 0);
	g_type_module_add_interface (module, avs_type, NAUTILUS_TYPE_MENU_PROVIDER, &menu_provider_iface_info);
}
