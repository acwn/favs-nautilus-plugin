/*

Filename: nautilus-favs.h
Copyright  2005 - 2013  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs/

This file is part of Filemanager-AVS.

Filemanager-AVS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Filemanager-AVS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Filemanager-AVS.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef __NAUTILUS_FAVS_H__
#define __NAUTILUS_FAVS_H__

/* Headers */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

/* Gnome headers */
#include <libgnomevfs/gnome-vfs.h>
#include <libgnomevfs/gnome-vfs-utils.h>

/* Nautilus headers */
#include <libnautilus-extension/nautilus-menu-provider.h>

/* Glib headers */
#include <glib/gi18n-lib.h>
#include <glib-object.h>

/* i18n headers */
#include <libintl.h>
#include <locale.h>
#include <string.h>

G_BEGIN_DECLS

typedef struct _FilemanagerAVS		FilemanagerAVS;
typedef struct _FilemanagerAVSClass	FilemanagerAVSClass;

#define FILEMANAGER_AVS_TYPE				(filemanager_avs_get_type ())
#define FILEMANAGER_AVS (object)			(G_TYPE_CHECK_INSTANCE_CAST ((object), FILEMANAGER_AVS_TYPE, FilemanagerAVS))
#define FILEMANAGER_IS_AVS (object)		(G_TYPE_CHECK_INSTANCE_TYPE ((object), FILEMANAGER_TYPE_AVS))

GType filemanager_avs_get_type (void);
void filemanager_avs_register_type (GTypeModule *module);

struct _FilemanagerAVS
{
	GObject parent_slot;
};

struct _FilemanagerAVSClass
{
	GObjectClass parent_slot;
};

G_END_DECLS

#endif
