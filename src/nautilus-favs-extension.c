/*

Filename: autilus-favs-extension.c
Copyright  2005 - 2013  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs/

This file is part of Filemanager-AVS.

Filemanager-AVS is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Filemanager-AVS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Filemanager-AVS.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "nautilus-favs.h"

void nautilus_module_initialize (GTypeModule *module)
{
	filemanager_avs_register_type (module);

	#ifdef ENABLE_NLS
	setlocale (LC_ALL, "");
	bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);
	#endif
}

void nautilus_module_shutdown (void)
{

}

void nautilus_module_list_types (const GType **types, int *num_types)
{
	static GType type_list [1];

	type_list[0] = FILEMANAGER_AVS_TYPE;
	*types = type_list;
	*num_types = G_N_ELEMENTS (type_list);
}
